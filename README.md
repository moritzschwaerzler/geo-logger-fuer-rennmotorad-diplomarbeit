# Geo-Logger für Rennmotorad  - Diplomarbeit

##  Allgemein
Im Motoradrennsport werden GL eingesetzt, um Trainingsfahrten und Rennen aufzuzeichnen. Diese Daten werden dann benutzt, um Rückschlüsse auf das Fahrverhalten zu ziehen. Diese Profigeräte sind jedoch sehr teuer und für Amateursportler kaum rentabel. Daher soll im Zuge dieser Diplomarbeit eine billigere und abgespeckte Variante entwickelt werden, welche zu einem leistbaren Preis, immer noch vernünftige Daten, mit einer hohen Datenrate liefert. 

### Blockschaltbild des Systems
![Blockschaltbild des Systems](pics/Blockschaltbild.png "Blockschaltbild des Systems")

### Testfahrt des Logger
![Testfahrt](pics/testfahrt.png "Testfahrt")


### Aufgabenstellung
Ziel ist es ein System zu entwerfen, welches GNSS-Daten abspeichern kann. Das Gerät soll im Rennmotorradsport verwendet werden und deshalb möglichst den Bedingungen standhalten können (Vibrationen, Stürze, Spritzwasser etc…). Um aussagekräftige Daten zu lesen, müssen diese mit einer hohen Samplerate aufgezeichnet werden (mindestens 10 Hz). Zudem sollte die Genauigkeit möglichst hoch sein (vor allem der relative Fehler muss möglichst klein sein). Weiter muss das Gerät eine Mindestlaufzeit von 3h Stunden aufweisen und wieder aufladbar sein. Der Zugriff auf das Gerät soll über eine Mikro-USB-Schnittstelle ermöglicht werden (das Laden soll auch über diese Schnittstelle erfolgen). Um die Daten vom GL zu lesen und diesen zu konfigurieren soll eine Desktopanwendung entwickelt werden. Die Steuerung des µC und somit des GL übernimmt die zu entwickelnde µC-Software.

### Umsetzung
Um GNSS Daten zu empfangen wurde ein Empfängermodul gewählt. Dies empfängt die GNSS Daten und berechnet daraus die Positionsdaten. Diese Daten werden dann über die UART Schnittstelle bereitgestellt. Um Daten zu speichern wird ein Flash-Baustein verwendet. Zur Vereinfachung der Kommunikation mit dem PC wird ein USB/UART-Wandler eingesetzt. Weiter wird ein Li-Ion-Akku verwendet, um das Gerät zu betreiben. Dadurch muss ein Lade-Controller eingesetzt werden, um ein korrektes Laden garantieren zu können. Da die Zellenspannung unter der Betriebsspannung liegt muss ein Boost-Konverter eingesetzt werden, um dieses Spannungslevel anzuheben.
 

## [zur schriftlichen Arbeit](Organisatorisches%20und%20Dokumentation/Geo-Logger-fuer-Rennmotorrad.pdf)


## kurzer Überblick:

<details><summary>[Hardware](Hardware)</summary>

<details><summary>[Prototyp 1](Hardware/v1)</summary>
Ziel hierbei war es jede Teilkomponente in Betrieb nehmen zu können.
Dies ist bis auf das Laden des Akkus erfolgreich umgesetzt worden.

### Layout

![Layout](pics/layout1.png "Testfahrt")


### 3D-Bild der Platine

![3D-Bild der Platine](pics/3d1.png "3D-Bild der Platine")


### Gehäuse

![Gehäuse](pics/gehaeuse1.jpg "Gehäuse")

</details>
<details><summary>[Prototyp 2](Hardware/v2)</summary>
Ziel hierbei war es alle Hardware-Anforderungen umzusetzten (außer Schutz gegen Wasser und Erschütterungen).
Dies konnte erfolgreich umgesetzt werden.

### Layout

![Layout](pics/layout2.png "Testfahrt")


### 3D-Bild der Platine

![3D-Bild der Platine](pics/3d2.png "3D-Bild der Platine")


### Gehäuse

![Gehäuse](pics/gehaeuse2.png "Gehäuse")


### Gesamtsystem im Gehäuse

![Gesamtsystem im Gehäuse](pics/sys2.png "Gesamtsystem im Gehäuse")

</details>

<details><summary>[Prototyp 3](Hardware/v3)</summary>
Bei diesem Prototypen wurde das System optimiert!
Die mechanische Wiederstandsfähigkeit (Schutz vor Spritzwasser, Vibrationen und Erschütterungen) kann trotzdem noch nicht gewährleistet werden.
Dieser Prototyp wurde nach der Abgabe der schriftlichen Arbeit entwickelt und ist deshalb nicht in der Dokumentation zu finden.

### Layout

![Layout](pics/layout3.png "Testfahrt")


### 3D-Bild der Platine

![3D-Bild der Platine](pics/3d3.png "3D-Bild der Platine")


### Gesamtsystem ohne Gehäuse

![Gesamtsystem ohne Gehäuse](pics/sys3-ohne-gehauese.png "Gesamtsystem im Gehäuse")

</details>
</details>

<details><summary>[Software](Software)</summary>
Für die Umsetzung des Projekts sind zwei Softwareteile notwendig:
<details><summary>[PC](Software/PC)</summary>
Mit der entwickelten Software soll auf den Geo-Logger (per PC angeschlossen) zugegriffen werden. So können die aufgezeichneten Daten als 	CSV-Datei lokal gespeichert werden. Zudem kann der Logger konfiguriert und der Speicher gelöscht werden.

</details>

<details><summary>[µC](Software/uC)</summary>
Steuert den µC und somit die Hardware des Logger
</details>
</details>

## zum Team

**Moritz Schwärzler**
Zuständig für die Projektleitung und das Projektmanagement. Zuständig für die Hardwareentwicklung und das Aufbauen. Zudem verantwortlich für die PC-Software.


**Sebastian Stieger**
Zuständig für die µC-Software. Hilfe leisten bei der Hardwareentwicklung und beim Aufbauen (vor allem bei Abstimmungen mit der µC-Software). Verantwortlich für Gehäuse und Mechanik.


Die Verifikation wird gemeinsam durchgeführt!





### Alles in allem war diese Diplomarbeit ein Erfolg!
