


/*** MACROS *******************************************************************/

#define F_CPU 8000000UL			// required for setbaud & other libraries
#define BAUD 9600L				// desired baud
#define BAUD_TOL 2					// desired baud rate tolerance (+/- %)

/*** INCLUDES *****************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <avr/io.h>					// SFR/Bit identifiers
#include <avr/interrupt.h  >			// ISR macro
#include <util/setbaud.h>


// R�ckmeldecodes
#define UART_ERR 0
#define UART_OK 1


// Einstellungen f�r die Timeouts
#define TIMEOUT_N_100_US 100

#define UART_CR 13
#define UART_LF 10


void USART0_Init(long baudrate);				// USART0 initialization
void USART0_Put(uint8_t data);		// Transmit a byte
void USART0_PutString(char *ptr,char sendCrLF);	// Transmit a string
void USART0_PutArray(char *ptr,char length,char sendCrLF);
char USART0_GetChar(void);			// Receive a character (if available)
uint8_t USART0_WaitForNewData(void);

void USART1_Init(long baudrate);				// USART0 initialization
void USART1_Put(uint8_t data);		// Transmit a byte
void USART1_PutString(char *ptr,char sendCrLF);	// Transmit a string
void USART1_PutArray(char *ptr,char length,char sendCrLF);

char USART1_GetChar(void);			// Receive a character (if available)
uint8_t USART1_WaitForNewData(void);