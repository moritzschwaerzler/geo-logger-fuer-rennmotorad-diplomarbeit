/*
 * Flash_Communication.h
 *
 * Created: 20/01/2021 08:29:06
 *  Author: Sebi
 */ 


/*
 * flas_communication.h
 *
 * Created: 08.12.2020 11:24:57
 *  Author: Sebi
 */ 
#include <avr/io.h>
#include <stdio.h>


void SPI_Masterinit();
void init_flash();
void write_byte_spi (char d);
char read_byte_spi (void);
void read_FlashID(char *deviceID);
void write_config(char config);
void read_flash(unsigned long Add,char *data,char length);
char read_byte_flash(unsigned long Add);
void write_flash(unsigned long Add,char *data,char length);
void write_byte_Flash(unsigned long Add,char data);
void chip_erase_flash();
void erase_entirechip();
char read_statusRegister();


 