/*
 * libaries.h
 *
 * Created: 28/02/2021 15:47:02
 *  Author: Sebi
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <string.h>
#include <avr/eeprom.h>//https://www.eit.lth.se/fileadmin/eit/courses/edi021/avr-libc-user-manual/group__avr__eeprom.html, https://exploreembedded.com/wiki/Permanent_Data_Storage_with_AVR_Internal_EEPROM


#include "Flash_Communication.h"
#include "GNSS_Comunication.h"
#include "uart.h"
#include "Memorry_Management.h"
#include "Search_Data.h"
#include "PC_Communication.h"
#include "data_logging.h"
#include "EEPROM_Communication.h"

#define F_CPU 8000000UL
