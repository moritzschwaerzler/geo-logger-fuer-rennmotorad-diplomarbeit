/*
 * EEPROM_Communication.c
 *
 * Created: 28/02/2021 14:56:14
 *  Author: Sebi
 */ 
#include "libaries.h"


void eeprom_write_string(char *data,uint8_t start_add, char length){
	char cnt=0;
	while(cnt<length)
	{
		eeprom_busy_wait();
		eeprom_write_byte((start_add+cnt),(data[cnt]));
		cnt++;
	}
}

void eeprom_read_string(char *data,uint8_t start_add, char length){
	char cnt=0;
		 
		 
	while(cnt<length)
	{
		eeprom_busy_wait();
		data[cnt]=(eeprom_read_byte(start_add+cnt));
		cnt++;
	}
		 
		 
}