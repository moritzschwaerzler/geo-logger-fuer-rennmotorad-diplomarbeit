/* 
 * flash_communication.c
 *
 * Created: 06.12.2020 10:00:00
 *  Author: Sebi
 */ 


#include "libaries.h"


#define DUMMY_BYTE 0x00 //Dummy Byte f�r ersten Transfer nach Reset


void SPI_Masterinit(){
	
	DDRE|=(1<<PINE3); //MOSI1
	DDRC|=(1<<PINC1); //SCK1
	DDRE|=(1<<PINE2); //SS1
	DDRC&=~(1<<PINC0); //MISO1
	
	PORTE|=(1<<PINE2); //SS1 pullup
	PORTC|=(1<<PINC0); //MISO1 pullup
	
	
	SPCR1 = (1<<SPE1)|(1<<MSTR1)|(1<<SPR10); //en spi, Mastermode, vorteiler =16
	
	//SPCR1|=(1<<SPI2X1);//doubl
	
	SPDR1=DUMMY_BYTE;//Dummy byte senden
	
	while(!(SPSR1 & (1 << SPIF1)));
}

 char spi(char d)
{
	
	SPDR1 = d;
	while(!(SPSR1 & (1 << SPIF)));
	//PORTD^=(1<<PIND2);

	return SPDR1;
}

void write_byte_spi (char d)
{
	spi(d);
}

char read_byte_spi (void)
{
	return spi(0x00);
}


void init_flash(){
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x06);//Wren
	PORTE|=(1<<PINE2); //SS1  High
	_delay_us(10);
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x01);//write to statusregister
	write_byte_spi(0x00);// all zero --> clear all writeprotection
	PORTE|=(1<<PINE2); //SS1  High
}


void read_FlashID(char *deviceID){
	
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x90);//oder AB
	write_byte_spi(0x00);//oder AB
	write_byte_spi(0x00);//oder AB
	write_byte_spi(0x00);//oder AB
	deviceID[0]=read_byte_spi();
	deviceID[1]=read_byte_spi();
	deviceID[2]=read_byte_spi();
	deviceID[3]=read_byte_spi();
	PORTE|=(1<<PINE2); //SS1  High
	
	
	
}

char read_statusRegister(){
	char statusregister;
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x05);//oder AB
	statusregister=read_byte_spi();
	PORTE|=(1<<PINE2); //SS1  High
	
	return(statusregister);
	
}



void read_flash(unsigned long Add,char *data,char length){
	char i=0;
	
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x03);//read befehl
	write_byte_spi((Add>>16)&0xff);
	write_byte_spi((Add>>8)&0xff);
	write_byte_spi((Add)&0xff);
	

	
	for(i=0;i<length;i++){
		data[i]=read_byte_spi();
	}
	PORTE|=(1<<PINE2); //SS1  High
	
	
	
}


char read_byte_flash(unsigned long Add){
	char data;
	
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x03);//read befehl
	write_byte_spi((Add>>16)&0xff);
	write_byte_spi((Add>>8)&0xff);
	write_byte_spi((Add)&0xff);
	

	
	
		data=read_byte_spi();
	
	PORTE|=(1<<PINE2); //SS1  High
	
	return(data);
	
}


void write_byte_Flash(unsigned long Add,char data){
	
	
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x06);//Wren
	PORTE|=(1<<PINE2); //SS1  High
	_delay_ms(5);
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x02);//write page
	write_byte_spi((Add>>16)&0xff);
	write_byte_spi((Add>>8)&0xff);
	write_byte_spi((Add)&0xff);
	
	write_byte_spi(data);
	PORTE|=(1<<PINE2); //SS1  High
	
	
	_delay_ms(5);
}

void write_flash(unsigned long Add,char *data,char length){//mindestl�nge 2 byte //startaddresse gerade
	char i=0;


	//PORTE&=~(1<<PINE2); //SS1  Low
	//write_byte_spi(0x70);//EBSY
	//PORTE|=(1<<PINE2); //SS1  High
	//_delay_ms(1);
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x06);//Wren
	PORTE|=(1<<PINE2); //SS1  High
	_delay_us(1);
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0xAD);//AAI_Word_Programm
	write_byte_spi((Add>>16)&0xff);
	write_byte_spi((Add>>8)&0xff);
	write_byte_spi((Add)&0xff);
	_delay_us(1);
	write_byte_spi(data[0]);
	write_byte_spi(data[1]);
	PORTE|=(1<<PINE2); //SS1  High
		_delay_us(10);
	
	for(i=2;i<length;i+=2){
		PORTE&=~(1<<PINE2); //SS1  Low
		write_byte_spi(0xAD);
		write_byte_spi(data[i]);
		if (i>(length-2))
		{
			write_byte_spi(0xFF);
		}
		else
		{
			write_byte_spi(data[i+1]);
		}
		PORTE|=(1<<PINE2); //SS1  High
		_delay_us(10);
		
		
	}

	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x04);//WRDI
	PORTE|=(1<<PINE2); //SS1  High
	
	//while(!(PINC & (1<<PINC0))) {;}
	//PORTE&=~(1<<PINE2); //SS1  Low
	//write_byte_spi(0x04);//WRDI
	//PORTE|=(1<<PINE2); //SS1  High
	//_delay_ms(1);
	//PORTE&=~(1<<PINE2); //SS1  Low
	//write_byte_spi(0x80);//DBSY
	//PORTE|=(1<<PINE2); //SS1  High

	
	
}



void erase_entirechip(){
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x06);
	PORTE|=(1<<PINE2); //SS1  High
	_delay_ms(5);
	PORTE&=~(1<<PINE2); //SS1  Low
	write_byte_spi(0x60);// 0der 0x60
	PORTE|=(1<<PINE2); //SS1  High
	_delay_ms(2000);
	}





