/*
 * Memorry_Management.c
 *
 * Created: 24/01/2021 17:23:25
 *  Author: Sebi
 */ 
#include "libaries.h"

#define datenbreite 12
#define pos_lat 9
#define pos_long 20
#define pos_time 0
#define pos_height
#define pos_date 0
#define pos_samplerate 8
#define pos_ind_EW 31
#define pos_ind_NS 19

#define length_long 11
#define length_lat 10
#define length_time 9
#define length_coordinate 11
#define length_height
#define length_date 6
#define length_samplerate 1




void save_data_string(char *data,char length,unsigned long datapoint){ //Alle daten einer nachricht abspeichern
	
	char Time[length_time];
	char longitude[length_long], latitude[length_lat];
	char data_string[12];
	char indicator_NS=0, indicator_WE=0;
	unsigned long longitude_=0, latitude_=0, time_=0;
	
	Search_for_time(data,length,Time);
	
	Search_for_Coordinates(data,length,longitude,latitude);
	indicator_NS=search_indicator_NS(data,length);
	indicator_WE=search_indicator_WE(data,length);
	
	time_=convert_time_asci_to_dec(Time);
	longitude_=convert_longitude_asci_to_dec(longitude);
	latitude_=convert_latitude_asci_to_dec(latitude);
	
	if (time_>=2000000000)
	{
	}
	else{
		if (longitude_>=2000000000)
		{
		//error
		}
		else{
		data_string[0]=((longitude_>>24)&0xff);
		data_string[1]=((longitude_>>16)&0xff);
		data_string[2]=((longitude_>>8)&0xff);
		data_string[3]=((longitude_)&0xff);
	
		if (indicator_WE==0x57)//wen "W"
		{
			data_string[0]|=(1<<7);
		}
		
		}
		
		if (latitude>=2000000000)
		{
			//error
		}
		else{
			data_string[4]=((latitude_>>24)&0xff);
			data_string[5]=((latitude_>>16)&0xff);
			data_string[6]=((latitude_>>8)&0xff);
			data_string[7]=((latitude_)&0xff);
			
			if (indicator_NS==0x4E)//wenn = "N"
			{
				data_string[4]|=(1<<7);
			}
			
		}
		data_string[8]=((time_>>24)&0xff);
		data_string[9]=((time_>>16)&0xff);
		data_string[10]=((time_>>8)&0xff);
		data_string[11]=((time_)&0xff);
	}


	
	
	
	unsigned long add=0;
	add=(datapoint*datenbreite);
	
	write_flash(add,data_string,12);
	

	
	memset(Time,0,length_time);
	memset(longitude,0,length_long);
	memset(latitude,0,length_lat);
	memset(data_string,0,12);
	
}

void get_data_string(unsigned long datapoint,char *longitude,char *latitude,char *Time,char *indicators){
	char data_string[12];
	char test[4];
	unsigned long longitude_=0,latitude_=0,time_=0;
	unsigned long add=0;
	add=(datapoint*datenbreite);
	
	read_flash(add,data_string,12);
	

	
	longitude_=(data_string[0]&(~(1<<7)))*16777216UL;
	longitude_+=data_string[1]*65536UL;
	longitude_+=data_string[2]*256UL;
	longitude_+=data_string[3];
	
	//test[0]=((longitude_>>24)&0xff);
	//test[1]=((longitude_>>16)&0xff);
	//test[2]=((longitude_>>8)&0xff);
	//test[3]=((longitude_)&0xff);
	//USART1_PutString("Test:",0);
	//USART1_PutArray(test,4,1);

	
	convert_longitude_dec_to_asci(longitude_,longitude);
	
	if ((data_string[0]&(1<<7))==(1<<7))
	{
		indicators[0]=0x57;
	}
	else{
		indicators[0]=0x45;
	}
	
	
	latitude_=(data_string[4]&(~(1<<7)))*16777216UL;
	latitude_+=data_string[5]*65536UL;
	latitude_+=data_string[6]*256UL;
	latitude_+=data_string[7];
	
	convert_latitude_dec_to_asci(latitude_,latitude);
	
	if ((data_string[4]&(1<<7))==(1<<7))
	{
		indicators[1]=0x4E;
	}
	else{
		indicators[1]=0x53;
	}
	
	time_=data_string[8]*16777216UL;
	time_+=data_string[9]*65536UL;
	time_+=data_string[10]*256UL;
	time_+=data_string[11];
	
	convert_time_dec_to_asci(time_,Time);
	
}


void write_samplerate(char samplerate,unsigned long datapoint){
	unsigned long add=0;
	
	add=(datapoint*datenbreite)+pos_samplerate;
	write_byte_Flash(add,samplerate);	//Daten vom Flash holen
}

char get_samplerate(unsigned long datapoint){
	char samplerate=0;
	unsigned long add=0;
	add=(datapoint*datenbreite)+pos_samplerate;//Adresse Berechnen
	
	
	samplerate=read_byte_flash(add);	//Daten vom Flash holen
	return(samplerate);
}

void write_Date (char *date ,unsigned long datapoint){
	unsigned long add=0;
	add=(datapoint*datenbreite)+pos_date;
	
	write_flash(add,date,length_date);
	
}

void get_Date (char *date ,unsigned long datapoint){
	unsigned long add=0;
	add=(datapoint*datenbreite)+pos_date;
	
	read_flash(add,date,length_date);
	
}


void save_log_header(char *data,char length,unsigned long datapoint){
	char samplerate=0;
	char date[7];
	eeprom_busy_wait();
	samplerate=eeprom_read_byte(Add_samplerate);
	search_for_date(data,length,date);
	write_Date(date,datapoint);
	write_samplerate(samplerate,datapoint);
	
	
	
	
}

unsigned long convert_longitude_asci_to_dec(char *longitude){
	unsigned long data_longitude=0;
	
		if (longitude[10]>47&&longitude[10]<58)
		{
			data_longitude= longitude[10]-48;
		}
		else{return(2*1000000000);}//error
		
		if (longitude[9]>47&&longitude[9]<58)
		{
			data_longitude+= (longitude[9]-48)*10UL;
		}
		else{return(2*1000000000);}//error
		
		if (longitude[8]>47&&longitude[8]<58)
		{
			data_longitude+= (longitude[8]-48)*100UL;
		}
		else{return(2*1000000000);}//error
		
		if (longitude[7]>47&&longitude[7]<58)
		{
			data_longitude+= (longitude[7]-48)*1000UL;
		}
		else{return(2*1000000000);}//error
		
		if (longitude[6]>47&&longitude[6]<58)
		{
			
			data_longitude+= ((longitude[6]-48)*10000UL);
		}
		else{return(2*1000000000);}//error
			
			//nicht an stelle 5 --> enth�lt nur komma punkt
			
		if (longitude[4]>47&&longitude[4]<58)
		{
			data_longitude+= (longitude[4]-48)*100000UL;
		}
		else{return(2*1000000000);}//error
		
		if (longitude[3]>47&&longitude[3]<58)
		{
			data_longitude+= (longitude[3]-48)*1000000UL;
		}
		else{return(2*1000000000);}//error
			
		if (longitude[2]>47&&longitude[2]<58)
		{
			data_longitude+= (longitude[2]-48)*10000000UL;
		}
		else{return(2*1000000000);}//error
			
		if (longitude[1]>47&&longitude[1]<58)
		{
			data_longitude+= (longitude[1]-48)*100000000UL;
		}
		else{return(2*1000000000);}//error
		
		if (longitude[0]>47&&longitude[0]<58)
		{
			data_longitude+= (longitude[0]-48)*1000000000UL;
		}
		else{return(2*1000000000);}//error
			
		return(data_longitude);	
}

void convert_longitude_dec_to_asci(unsigned long longitude_dec, char *longitude){
	
	unsigned long temp=0;
	longitude[10]=(longitude_dec%10)+48;
	temp=longitude_dec-longitude_dec%10;
	
	longitude[9]=(temp%100)/10+48;
	temp=longitude_dec-longitude_dec%100;
	
	longitude[8]=(temp%1000)/100+48;
	temp=longitude_dec-longitude_dec%1000;
	
	longitude[7]=(temp%10000)/1000+48;
	temp=longitude_dec-longitude_dec%10000;
	
	longitude[6]=(temp%100000)/10000+48;
	temp=longitude_dec-longitude_dec%100000;
	
	longitude[5]=0x2E;//komma-punkt
	
	longitude[4]=(temp%1000000)/100000+48;
	temp=longitude_dec-longitude_dec%1000000;
	
	longitude[3]=(temp%10000000)/1000000+48;
	temp=longitude_dec-longitude_dec%10000000;
	
	longitude[2]=(temp%100000000)/10000000+48;
	temp=longitude_dec-longitude_dec%100000000;
	
	longitude[1]=(temp%1000000000)/100000000+48;
	temp=longitude_dec-longitude_dec%1000000000;
	
	longitude[0]=temp/1000000000+48;
}

unsigned long convert_latitude_asci_to_dec(char *latitude){
	unsigned long data_latitude=0;
	
	if (latitude[9]>47&&latitude[9]<58)
	{
		data_latitude= latitude[9]-48;
	}
	else{return(2*1000000000);}//error
	
	if (latitude[8]>47&&latitude[8]<58)
	{
		data_latitude+= (latitude[8]-48)*10UL;
	}
	else{return(2*1000000000);}//error
	
	if (latitude[7]>47&&latitude[7]<58)
	{
		data_latitude+= (latitude[7]-48)*100UL;
	}
	else{return(2*1000000000);}//error
	
	if (latitude[6]>47&&latitude[6]<58)
	{
		data_latitude+= (latitude[6]-48)*1000UL;
	}
	else{return(2*1000000000);}//error
	
	if (latitude[5]>47&&latitude[5]<58)
	{
		data_latitude+= (latitude[5]-48)*10000UL;
	}
	else{return(2*1000000000);}//error
	
	//nicht an stelle 4 --> enth�lt nur komma punkt
	
	if (latitude[3]>47&&latitude[3]<58)
	{
		data_latitude+= (latitude[3]-48)*100000UL;
	}
	else{return(2*1000000000);}//error
	
	if (latitude[2]>47&&latitude[2]<58)
	{
		data_latitude+= (latitude[2]-48)*1000000UL;
	}
	else{return(2*1000000000);}//error
	
	if (latitude[1]>47&&latitude[1]<58)
	{
		data_latitude+= (latitude[1]-48)*10000000UL;
	}
	else{return(2*1000000000);}//error
	
	if (latitude[0]>47&&latitude[0]<58)
	{
		data_latitude+= (latitude[0]-48)*100000000UL;
	}
	else{return(2*1000000000);}//error
	
	return(data_latitude);
}

void convert_latitude_dec_to_asci(unsigned long latitude_dec, char *latitude){
	
	unsigned long temp=0;
	latitude[9]=(latitude_dec%10)+48;
	temp=latitude_dec-latitude_dec%10;
	
	latitude[8]=(temp%100)/10+48;
	temp=latitude_dec-latitude_dec%100;
	
	latitude[7]=(temp%1000)/100+48;
	temp=latitude_dec-latitude_dec%1000;
	
	latitude[6]=(temp%10000)/1000+48;
	temp=latitude_dec-latitude_dec%10000;
	
	latitude[5]=(temp%100000)/10000+48;
	temp=latitude_dec-latitude_dec%100000;
	
	latitude[4]=0x2E;//komma-punkt
	
	latitude[3]=(temp%1000000)/100000+48;
	temp=latitude_dec-latitude_dec%1000000;
	
	latitude[2]=(temp%10000000)/1000000+48;
	temp=latitude_dec-latitude_dec%10000000;
	
	latitude[1]=(temp%100000000)/10000000+48;
	temp=latitude_dec-latitude_dec%100000000;
	
	latitude[0]=temp/100000000+48;
}




unsigned long convert_time_asci_to_dec(char *time){
	unsigned long data_time=0;
	
	if (time[8]>47&&time[8]<58)
	{
		data_time= time[8]-48;
	}
	else{return(2*1000000000);}//error
	
	
	if (time[7]>47&&time[7]<58)
	{
		data_time+= (time[7]-48)*10UL;
	}
	else{return(2*1000000000);}//error
		
		//nicht an stelle 6 --> enth�lt nur komma punkt
	
	if (time[5]>47&&time[5]<58)
	{
		data_time+= (time[5]-48)*100UL;
	}
	else{return(2*1000000000);}//error
	
	if (time[4]>47&&time[4]<58)
	{
		data_time+= (time[4]-48)*1000UL;
	}
	else{return(2*1000000000);}//error
	
	if (time[3]>47&&time[3]<58)
	{
		data_time+= (time[3]-48)*10000UL;
	}
	else{return(2*1000000000);}//error
	
	if (time[2]>47&&time[2]<58)
	{
		data_time+= (time[2]-48)*100000UL;
	}
	else{return(2*1000000000);}//error
	
	if (time[1]>47&&time[1]<58)
	{
		data_time+= (time[1]-48)*1000000UL;
	}
	else{return(2*1000000000);}//error
	
	if (time[0]>47&&time[0]<58)
	{
		data_time+= (time[0]-48)*10000000UL;
	}
	else{return(2*1000000000);}//error
	
	return(data_time);
}

void convert_time_dec_to_asci(unsigned long time_dec, char *time){
	
	unsigned long temp=0;
	time[8]=(time_dec%10)+48;
	temp=time_dec-time_dec%10;
	
	time[7]=(temp%100)/10+48;
	temp=time_dec-time_dec%100;
	
	time[6]=0x2E;//komma-punkt
	
	time[5]=(temp%1000)/100+48;
	temp=time_dec-time_dec%1000;
	
	time[4]=(temp%10000)/1000+48;
	temp=time_dec-time_dec%10000;
	
	time[3]=(temp%100000)/10000+48;
	temp=time_dec-time_dec%100000;
	
	time[2]=(temp%1000000)/100000+48;
	temp=time_dec-time_dec%1000000;
	
	time[1]=(temp%10000000)/1000000+48;
	temp=time_dec-time_dec%10000000;
	
	time[0]=temp/10000000+48;
}