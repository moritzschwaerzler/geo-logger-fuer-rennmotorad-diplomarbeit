/*
 * comunication_gnss.h
 *
 * Created: 23.11.2020 16:27:07
 *  Author: 160113
 */ 

#define Header1 0xB5
#define Header2 0x62







#include <avr/io.h>

#include "uart.h"
#include "string.h"

#include <avr/interrupt.h>


char search_message(char *type,char *recived);
char GNSS_read_string(char *data);
char checksum( char *buf, int cnt);
int convert_asciinumber_to_actualnumber(char n1,char n2);
char check_checksumm(char *buf, int length);
void send_config(char *data, char len);
void configure_CFG_Rate(int periode, char navRate, char TimeRef);