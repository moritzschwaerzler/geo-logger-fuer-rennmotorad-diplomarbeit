/*
 * CFile1.c
 *
 * Created: 20/01/2021 17:38:01
 *  Author: Sebi
 */ 

#include "libaries.h"









char PC_read_string(char *data){ //liest die empfangener string von PC
	char length=0;
	char x=0, e=0;
	x=0;
	
	memset(data, 0, sizeof data);
	
	
	while (e<2)
	{
		
		
		
		
		if (USART1_WaitForNewData())
		{
			
			
			data[x]=USART1_GetChar();
			
			
			
			if (data[0]==0x3F||data[0]==0x21)
			{
				
				if ( e==0 && data[x]==0x0D)
				{
					e=1;
				}
				else if (e == 1 && data[x]==0x0A )
				{
					e=2;
					length=x;
				}
				else e =0;
				
				x++;
			}
			else{
				
				x=0;
				e=0;
			}
		}
		else{
			
			return(0);
			}
			
	}
	
	
	
	e=0;
	
	process_pc_message(data);
	
}















void process_pc_message(char *pc_data){
char name[11];	
char samp_rate[5];
	
	if (pc_data[0]==0x21){// configuration  wen "!"
		if (pc_data[1]==0x43&&pc_data[2]==0x4c&&pc_data[3]==0x45&&pc_data[4]==0x41&&pc_data[5]==0x52)//wenn befehl CLEAR --> flash speicher l�schen
		{
			erase_entirechip();
			eeprom_busy_wait();
			eeprom_write_byte((ADD_number_of_logs),0);//lognumber wieder auf 0 setzen
			eeprom_busy_wait();
			eeprom_write_dword((Add_current_datapoint),0);//datapoitner 0 setzen
			USART1_PutString("ACK",1);
			USART1_PutString("$",1);
		}
		
		else if (pc_data[1]==0x4e&&pc_data[2]==0x61&&pc_data[3]==0x6d&&pc_data[4]==0x65&&pc_data[5]==0x3d)//Wenn befehl NAME --> name des loggers �ndern
		{
			char e=0,cnt=0,cnt1=0;
			while (e<2)
			{
				if (pc_data[cnt]==0x3d||pc_data[cnt]==0x0D||pc_data[cnt]==0x0A)e++;//wen = oder cr oder lf
			
				
				else if (e==1)
				{
					name[cnt1]=pc_data[cnt];
					cnt1++;
				}
				
				cnt++;
			}
			
		
			eeprom_write_string(name,(Add_Device_Name),(cnt1));
			
			eeprom_busy_wait();
			
			eeprom_write_byte((Add_Device_Name+10),(cnt1));
			eeprom_busy_wait();
			//write name to eeprom
			
			
			USART1_PutString("ACK",1);
			USART1_PutString("$",1);
			cnt=0;
			cnt1=0;
			
		}
		else if (pc_data[1]==0x53&&pc_data[2]==0x52)//befehl SR --> configure sample rate
		{
			
			
			switch(pc_data[4])
			{
				case 0x31:
					if (pc_data[5]==0x30)
					{
						configure_CFG_Rate(100,1,1);
					}
					else{
						configure_CFG_Rate(1000,1,1);
					}
					break;
				case 0x32:
					configure_CFG_Rate(500,1,1);
					break;
				case 0x33:
					configure_CFG_Rate(333,1,1);
					break;
				case 0x34:
					configure_CFG_Rate(250,1,1);
					break;
				case 0x35:
					configure_CFG_Rate(200,1,1);
					break;
				case 0x36:
					configure_CFG_Rate(167,1,1);
					break;
				case 0x37:
					configure_CFG_Rate(143,1,1);
					break;
				case 0x38:
					configure_CFG_Rate(125,1,1);
					break;
				case 0x39:
					configure_CFG_Rate(111,1,1);
					break;
				default:
					configure_CFG_Rate(1000,1,1);
					break;
			}
			
			
			if ((pc_data[4]==0x31)&&(pc_data[5]==0x30))
			{
				eeprom_write_byte((Add_samplerate),10);
			}
			else{
				eeprom_write_byte((Add_samplerate),pc_data[4]-0x30);
			}
			
			
			
			//not finished
			//configure_CFG_Rate();
			USART1_PutString("ACK",1);
			USART1_PutString("$",1);
		}
		else{
			USART1_PutString("NACK",1);
			USART1_PutString("$",1);
		}
	
	}
	else if (pc_data[0]==0x3F)//anfrage wen "?"
	{
		
		
		if (pc_data[1]==0x55)//wen Befel "U" ("wer bist du")
		{
			
			char var_name[11];
			char ID[6];
			char name_len=0;
			char samplerate;
			char asci_samplerate[2];
			memset(var_name,0,11);
			
			
			eeprom_busy_wait();
			name_len=eeprom_read_byte((Add_Device_Name+10));  
			eeprom_read_string(var_name,Add_Device_Name,name_len);
			 
			 eeprom_busy_wait();
			 samplerate=eeprom_read_byte(Add_samplerate);
			 convert_dec_Number_to_2stellig_ascii(samplerate,asci_samplerate);
			 
			eeprom_busy_wait();
			eeprom_read_string(ID,Add_Device_ID,6);
			 
			USART1_PutString("ID=",0);
			USART1_PutArray(ID,6,1);
			USART1_PutString("Konfig=",0);
			USART1_PutArray(var_name,name_len,0);
			USART1_PutString(";",0);
			USART1_PutArray(asci_samplerate,2,1);
			USART1_PutString("$",1);
			
			
		}
		if (pc_data[1]==0x41&&pc_data[2]==0x6C&&pc_data[3]==0x6C)//wen Befehl "ALL"
		{
			
			write_data_to_PC();
			
		}
		
		
	}
	else{
		USART1_PutString("NACK",1);
		USART1_PutString("$",1);
	}
	
}



void write_data_to_PC(){ //die gesamten daten vom Flash an den PC schicken, // Vieleicht noch startpunkt ausw�hlen
	
	unsigned long datapoint1=0,next_start_datapoint=0;
	char i=0;
	char coordinate_long[11],coordinate_lat[10], Time[9],indicators[2];
	char indicator_NS=0,indicator_WE;
	char number_of_logs =0;
	
	number_of_logs=eeprom_read_byte(ADD_number_of_logs);
	
	for (i=0;i<number_of_logs;i++)
	{
		
		
	
		write_header(datapoint1);
		datapoint1++;
		
		
		if (i==(number_of_logs-1))
		{
			next_start_datapoint=eeprom_read_dword(Add_current_datapoint);
		}
		else{
		next_start_datapoint=eeprom_read_dword((ADD_first_log_pointer+4*(i+1)));
		}
		
		
		while (datapoint1<next_start_datapoint)//f�r alle datenbl�cke
		{
			USART1_PutString("Data=",0);
			
			get_data_string(datapoint1,coordinate_long,coordinate_lat,Time,indicators);
			
			USART1_PutArray(coordinate_long,11,0);//l�ngengrad
			USART1_PutString(";",0);
			
			USART1_PutArray(coordinate_lat,10,0);//breitengrad
			USART1_PutString(";",0);
			
			USART1_PutString("500;",0);//H�he raus
			
			USART1_Put(indicators[1]);// nur indikator N/S
			USART1_PutString(";",0);
			
			USART1_Put(indicators[0]);// nur indikator W/E
			USART1_PutString(";",0);
			
			
		
			USART1_Put(Time[0]);
			USART1_Put(Time[1]);
			USART1_PutString(":",0);
			USART1_Put(Time[2]);
			USART1_Put(Time[3]);
			USART1_PutString(":",0);
			USART1_Put(Time[4]);
			USART1_Put(Time[5]);
			USART1_Put(Time[6]);
			USART1_Put(Time[7]);
			USART1_Put(Time[8]);
			USART1_PutString(";",1);
			
			
			datapoint1++;
		}
		
		
	}
	
	
	
	
	
	
	
	
	  
	USART1_PutString("$",1);
	
	
	
}

void write_header(unsigned long datapoint){
	
	char samplerate=0;
	char date[7],samplerate_array[2];
	
	get_Date(date,datapoint);
	samplerate=get_samplerate(datapoint);
	convert_dec_Number_to_2stellig_ascii(samplerate,samplerate_array);
	
	
	USART1_PutString("H=",0);
	USART1_Put(date[0]);
	USART1_Put(date[1]);
	USART1_PutString(".",0);
	USART1_Put(date[2]);
	USART1_Put(date[3]);
	USART1_PutString(".20",0);
	USART1_Put(date[4]);
	USART1_Put(date[5]);
	USART1_PutString(";",0);
	USART1_Put(samplerate_array[0]);
	USART1_Put(samplerate_array[1]);
	USART1_PutString(";",1);
}


void convert_dec_Number_to_2stellig_ascii(char number,char *ascinumber){
	if (number < 10)
	{
		ascinumber[0]=48;//=0
		ascinumber[1]=number+48;
	}
	else if (number<100&&number>9)
	{
		ascinumber[0]=((number-number%10)/10)+48;
		ascinumber[1]=(number%10)+48;
	}
	else{;}//error
}


convert_asciinumber_to_decnumber_4stellen(char n1,char n2, char n3, char n4){ //not done yet
	char new=0;
	if (n1>47&&n1<58)
	{
		new= n1-48;
	}
	else{
		new= (n1-55)*16;
	}
	
	if (n2>47&&n2<58)
	{
		new+= (n2-48);
	}
	else{
		new+= (n2-55);
	}
	
	
	return(new);
}