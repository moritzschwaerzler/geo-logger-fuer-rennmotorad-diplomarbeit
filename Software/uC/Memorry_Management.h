/*
 * Memorry_Management.h
 *
 * Created: 24/01/2021 17:23:49
 *  Author: Sebi
 */ 
#include <stdint.h>
#include <stdio.h>
#include <avr/io.h>



void get_longitude(char *longitude,unsigned long datapoint);
char get_Indicator_EW(unsigned long datapoint);
void get_latitude(char *latitude,unsigned long datapoint);
char get_Indicator_NS(unsigned long datapoint);
void get_Time(char *Time,unsigned long datapoint);
void write_longitude(char *longitude,unsigned long datapoint);
void write_Indicator_EW(char indicator_EW,unsigned long datapoint);
void write_latitude(char *latitude,unsigned long datapoint);
void write_Indicator_NS(char indicator_NS,unsigned long datapoint);
void write_Time (char *Time ,unsigned long datapoint);
void get_Height(char *height,unsigned long datapoint);
void save_log_header(char *data,char length,unsigned long datapoint);

void write_samplerate(char samplerate,unsigned long datapoint);
char get_samplerate(unsigned long datapoint);
void write_Date (char *date ,unsigned long datapoint);
void get_Date (char *date ,unsigned long datapoint);


unsigned long convert_longitude_asci_to_dec(char *longitude);
void convert_longitude_dec_to_asci(unsigned long longitude_dec, char *longitude);

unsigned long convert_latitude_asci_to_dec(char *latitude);
void convert_latitude_dec_to_asci(unsigned long latitude_dec, char *latitude);

unsigned long convert_time_asci_to_dec(char *time);
void convert_time_dec_to_asci(unsigned long time_dec, char *time);

void get_data_string(unsigned long datapoint,char *longitude,char *latitude,char *Time,char *indicators);
void save_data_string(char *data,char length,unsigned long datapoint);