/*
 * EEPROM_Communication.h
 *
 * Created: 28/02/2021 14:56:34
 *  Author: Sebi
 */ 



#define Add_Device_Name 0 //10 Byte -->12 because 4 byte sections // name of the device //device name add + 10 -->device name length
#define Add_Device_ID 12 //6 stellige zahl for now (8 byte reserved) //ID of the device
#define Add_samplerate 20 //current samplerate saved
#define Add_current_datapoint 40 //4 byte reserved  //the first datapoint (to start the first log)
#define ADD_number_of_logs 44 //4 byte reserved (probably only needs one) // tells how many logs have been saved
#define ADD_first_log_pointer 48// pointer of the first log , to get the pointers of the next log take this adress and add ((number of log -1) *4)

void eeprom_write_string(char *data,uint8_t start_add, char length);
void eeprom_read_string(char *data,uint8_t start_add, char length);

////eeprom Status
//eeprom_is_ready();//returns 1 when eeprom is ready
//eeprom_busy_wait(); //warten bis eeprom frei
//
////read data
//data=eeprom_read_byte(addr);
//data=eeprom_read_dword(addr);
//
////write data
//eeprom_write_dword(); //write a uint32_t to eeprom
//eeprom_write_byte();//write a byte to eeprom
//
//
//
////Update data
//eeprom_update_dword(); //write a uint32_t to eeprom
//eeprom_update_byte();//write a byte to eepr