

#include "libaries.h"

void USART0_Init(long baudrate){
	
	// Set the BAUD rate
	
	
	
	unsigned long UBRRCALC;
	
	UBRRCALC=F_CPU/(16*baudrate)-1;

	UBRR0H = (UBRRCALC >> 8);
	UBRR0L = (UBRRCALC & 0xff);
	
	
	// Set the Mode & Frame Parameters
	// Asynchronous, 8-data, No parity, 1 stop
	UCSR0C = 0x06;
	
	// Enable USART0 Transmitter and Receiver
	UCSR0B = (1 << TXEN0) | (1 << RXEN0);
 
} // USART0_Init()



void USART1_Init(long baudrate){
	
	// Set the BAUD rate
	
	unsigned long UBRRCALC;
	
	UBRRCALC=F_CPU/(16*baudrate)-1;
	
	UBRR1H = (UBRRCALC >> 8);
	UBRR1L = (UBRRCALC & 0xff);
	
	// Set the Mode & Frame Parameters
	// Asynchronous, 8-data, No parity, 1 stop
	UCSR1C = 0x06;
	
	// Enable USART0 Transmitter and Receiver
	UCSR1B = (1 << TXEN1) | (1 << RXEN1);
	
	UCSR1A|=(1<<1);//Double speed mode
	

} // USART0_Init()







void USART0_Put(uint8_t data){
	
	//Checking to see if USART TX buffer is empty for new data
	while(!(UCSR0A & (1<<UDRE0)));
	
	//Initiating transfer
	UDR0 = data;

} 




void USART1_Put(uint8_t data){
	
	//Checking to see if USART TX buffer is empty for new data
	while(!(UCSR1A & (1<<UDRE1)));
	
	//Initiating transfer
	UDR1 = data;

}

/*** USART0_PutString() ********************************************************/

void USART0_PutString(char *ptr,char sendCrLF){
	
	while(*ptr){			// Loop until end of string (*s = '\0')
		USART0_Put(*ptr++);	// Send the character and point to the next one
	}
	
	if (sendCrLF>0)
	{
		USART0_Put(0x0d);
		USART0_Put(0x0a);
		
	}
}

void USART1_PutString(char *ptr,char sendCrLF){
	
	while(*ptr){			// Loop until end of string (*s = '\0')
		USART1_Put(*ptr++);	// Send the character and point to the next one
	}
	
	if (sendCrLF>0)
	{
		USART1_Put(0x0d);
		USART1_Put(0x0a);
		
	}
	
}

void USART0_PutArray(char *ptr,char length,char sendCrLF){//Sendet array ohne 0 am Schuss
	char cnt=0;
	while(cnt-length){
		USART0_Put(ptr[cnt]);	// Send the character and point to the next one
		cnt++;
	}
	
	if (sendCrLF>0)
	{
		USART0_Put(0x0d);
		USART0_Put(0x0a);
		
	}
}


void USART1_PutArray(char *ptr,char length,char sendCrLF){//Sendet array ohne 0 am Schuss
	char cnt=0;
	while(cnt-length){			
		USART1_Put(ptr[cnt]);	// Send the character and point to the next one
		cnt++;
	}
	
	if (sendCrLF>0)
	{
		USART1_Put(0x0d);
		USART1_Put(0x0a);
		
	}
}




/*** USART0_GetChar() **************************************************************/

char USART0_GetChar(void){
	
	char rxdata;
	
	if(UCSR0A & (1<<RXC0)){		// checking if USART RX data is available
		rxdata = UDR0;			// reading the received byte (clears RXC0)
		return rxdata;			// return the data
	}
	
	return 0x00;				// return NUL char ('/0') if no data available

} 

char USART1_GetChar(void){
	
return UDR1;

}


uint8_t _loc_Rx0Complete(void)
{
	return ((UCSR0A & (1<<RXC0)) != 0);
}


uint8_t USART0_WaitForNewData(void)
{
	uint8_t TimeOutCnt=0;
	uint8_t RxOk=0;
	
	while ((TimeOutCnt<TIMEOUT_N_100_US) && !RxOk)
	{
		RxOk=_loc_Rx0Complete();
		_delay_us(10);
		TimeOutCnt++;
	}
	
	if (RxOk) return UART_OK;
	else return UART_ERR;
}


uint8_t _loc_Rx1Complete(void)
{
	return ((UCSR1A & (1<<RXC1)) != 0);
}


uint8_t USART1_WaitForNewData(void)
{
	uint8_t TimeOutCnt=0;
	uint8_t RxOk=0;
	
	while ((TimeOutCnt<TIMEOUT_N_100_US) && !RxOk)
	{
		
		RxOk=_loc_Rx1Complete();
		_delay_us(100);
		TimeOutCnt++;
	}
	
	if (RxOk) return UART_OK;
	
	else return UART_ERR;
}