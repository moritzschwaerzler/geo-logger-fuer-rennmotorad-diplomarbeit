/*
 * GNSS_V4.c
 *
 * Created: 28/02/2021 11:34:50
 * Author : Sebi
 */ 



#include "libaries.h"

#define F_CPU 8000000UL
volatile char rxdata;
volatile char mode=0, old_mode=0;
volatile char recived_data_pc[10];
volatile char cnt1=0;
volatile unsigned long datapoint_gnss_data=1;

#define counter_top 3

int main(void)
{
	
	
	
	
	 // Initialization
	 USART0_Init(9600);
	 USART1_Init(55000);//highest baudrate possible ~  55k*2 = 110k  --> doublespeed mode
	SPI_Masterinit();
	init_flash();

	
	sei();//interupts freigeben
	
	//Pinchange interupt
	PCMSK0|=(1<<PCINT1);//enable pinchange interupt on PCINT1 
	PCICR|=(1<<PCIE0);//enable Pin chnage interupt 0
	
	//TImer interupt
	TIMSK0|=(1<<TOIE0);//overflow enable
	
	
	DDRD|=(1<<PIND2)|(1<<PIND3)|(1<<PIND4);//LeDS as output
	DDRB&=~(1<<PINB1);//Taster als intput
	PORTB|=(1<<PINB1);//Default highpegel am taster
	
	//Config ADC
	ADMUX |= (1<<REFS1) |  (1<<MUX1) ;//Vref = VCC, Source ADC2
	ADCSRA |= (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0) | (1<<ADEN);
	
	char kek=0;
	char Time[9]="199999.13",longitude[11]="12345.49999",latitude[10]="1234.12345";
	char newlongitude[11],newlatitude[10],newTime[9];

	char gnssdata[80]="$GPRMC,122434.00,A,4724.57828,N,00945.78770,W,4.492,340.26,261020,,,A*61";
	gnssdata[72]=0x0D;
	gnssdata[73]=0x0A;
	char deviceID[4];
	char recived_message_pc[20];
	

	
	
	
	char dataout[10];
	char data[10];
	data[0]=0x91;
	data[1]=0x11;
	data[2]=0xa1;
	data[3]=0xb1;
	data[4]=0xcc;
	data[5]=0x93;
	data[6]=0x11;
	data[7]=0xa1;
	data[8]=0xb1;
	data[9]=0xcc;
	uint32_t add=2024;
	
	
	char x=0;
	char e=0;
	
	
	//erase_entirechip();
	//eeprom_busy_wait();
	//eeprom_write_byte((ADD_number_of_logs),0);//lognumber wieder auf 0 setzen
	//eeprom_busy_wait();
	//eeprom_write_dword((Add_current_datapoint),0);//datapoitner 0 setzenerase_entirechip();
	
	PORTD|=(1<<PIND3);
	
	//eeprom_write_string("183456",Add_Device_ID,6);
	
	
	while(1)	
	{

	
	
	if (mode==1)
	{
	
		PORTD|=(1<<PIND2);
		PORTD&=~(1<<PIND4);
		
		
		if (old_mode==0)//wen modewechsel
		{
			old_mode=mode;
			
			
		
			log_data(1);//log starten
			
			
			
		}
		else{
			log_data(0);
		}
	
		
			
			
		
	}
		
	else
		{
		
		

			if (old_mode==1)//wen modewechsel
			{
				old_mode=mode;
				
				
				eeprom_busy_wait();
				eeprom_write_dword((Add_current_datapoint),get_datapoint());//save datapoint for next logging
				
				
			}
			PORTD|=(1<<PIND4);//mode LED on
			PORTD&=~(1<<PIND2);

			PC_read_string(recived_message_pc);
			
		
			
		
		}	
		
		
		
	
	
		}
		
		
	}
	
	
	
	
	
	
	
	


ISR(PCINT0_vect){//Start taster interupt
	PCICR&=~(1<<PCIE0);//disable Pin chnage interupt 0//isr deaktivieren

	
	TCCR0B|=(1<<CS02)|(1>>CS00);//clk devider 1024
	
	
	
}

ISR(TIMER0_OVF_vect){
	static unsigned int count=0;
	if(count<counter_top)
	{
		if ((PINB & (1<<PINB1))){
			
			count=0;
			PCICR|=(1<<PCIE0);//enable Pin chnage interupt 0//isr wieder aktivieren
			TCCR0B&=~((1<<CS01)|(1<<CS02)|(1>>CS00));//Timer 0 stoppen
		}
	}
	else if (count==counter_top){
		mode^=1;	//mode wird umgeschaltet
		
	} 
		 
	else if ((PINB & (1<<PINB1)))
		{
			if ((count-counter_top)>30){
			count=0;
			PCICR|=(1<<PCIE0);//enable Pin chnage interupt 0//isr wieder aktivieren
			TCCR0B&=~((1<<CS01)|(1<<CS02)|(1>>CS00));//Timer 0 stoppen	
			}
			
		}
	
	count++;
	
	
}