/*
 * Search_Data.h
 *
 * Created: 20/01/2021 16:59:26
 *  Author: Sebi
 */ 


void Search_for_time(char *data,char length, char *Time);
void Search_for_Coordinates(char *data,char length, char *longitude, char *latitude);
char search_indicator_NS(char *data,char length);
char search_indicator_WE(char *data,char length);
void search_for_date(char *data, char length, char *date);