/*
 * comunications_gnss.c
 *
 * Created: 23.11.2020 16:42:39
 *  Author: 160113
 */ 

#include "libaries.h"



char GNSS_read_string(char *data){ //liest die kommunikatiobn vom gps modul aus
	char length=0;
	char x=0, e=0;
	x=0;
	
	memset(data, 0, sizeof data);
	
	
	
	while (e<2)
	{
		
		if (USART0_WaitForNewData())
		{
			
			
			
			data[x]=USART0_GetChar();
			
			
			
			if (data[0]==0x24)
			{
				//if (x==3&&data[3]==0x52);
				//{
					//USART1_PutArray(data,5,1);
					////return(0);
				//}
				
				
				
				if ( e==0 && data[x]==0x0D)
				{
					e=1;
				}
				else if (e == 1 && data[x]==0x0A )
				{
					e=2;
					length=x+1;
				}
				else e =0;
				
				x++;
			}
			else{
				memset(data, 0, 70);
				x=0;
				e=0;
			}
		}
	}
	
	
	e=0;
	
	return(length);
	
}


char search_message(char *type,char *recived){ //+berpr�ft ob die nachricht vom gew�nschten typ ist
	
	
	
	if (recived[0]==0x24){
		if(recived[3]==type[0]){
			if(recived[4]==type[1]){
				if(recived[5]==type[2]){
					return(1);
				}
			}
		}
	}
	
	
	
	return(0);
}



char checksum( char *buf, int cnt){ //berechnet die Ckecksummm (NMEA Standart)
	 char Character;
    int Checksum = 0;
    int i;              

    for (i=0;i<cnt;++i)
    {
        Character = buf[i];
        switch(Character)
        {
            case '$':  //$ zeichen ignorieren
                
                break;
            case '*': //* als letztes zeichen (*wird nicht mit eingerechnet
               
                i = cnt;
                continue;
            default:
                // Is this the first value for the checksum?
                if (Checksum == 0)
                {
                    // Yes. Set the checksum to the value
                    Checksum = Character;
					
                }
                else
                {
                    // No. XOR the checksum with this character's value
                    Checksum = Checksum ^ Character;
					
                }
                break;
        }
    }
	;
    // Return the checksum
    return (Checksum);
	
}

int convert_asciinumber_to_actualnumber(char n1,char n2){  //convertiert eine asci geschriebene 2 zeichen lange hexadezimal nummer in eine tats�chluiche hexadezimalnummer
	char new=0;
	if (n1>47&&n1<58)
	{
		new= (n1-48)*16;
	}
	else{
		new= (n1-55)*16;
	}
	
	if (n2>47&&n2<58)
	{
		new+= (n2-48);
	}
	else{
		new+= (n2-55);
	}
	
	
	return(new);
}

char check_checksumm(char *buf, int length){ //Vergleicht die Angeh�ngte checksumm mit der Berechneteten (NMEA Standart)
	
	
	
	if (checksum(buf,length)==convert_asciinumber_to_actualnumber(buf[length-4],buf[length-3]))
	{
		return(1);
		
	}
	else
	{
		return(0);
		
	}
}




void send_config(char *data, char len){//Konfiguration an gnss schicken
	char send[64];
	char CK_A = 0, CK_B = 0;
	
	for(char I=0;I<len;I++)//Generieren der UBX Cheksumm
	{
		CK_A = CK_A + data[I];
		CK_B = CK_B + CK_A; 
	}
	
	send[0]=Header1;//Header
	send[1]=Header2;
	
	for(char I=0;I<len;I++)
	{
		send[I+2]=data[I];
	}
	send[len+2]=CK_A;
	send[len+3]=CK_B;//Pr�fsumme anf�gen 

	USART0_PutArray(send,(len+4),1);

	
}


void configure_CFG_Rate(int periode, char navRate, char TimeRef){//Kpnfiguration der Update rate
	char Data[10];
	char a=0,b=0;
	
	
	Data[0]=0x06;//Klasse der nachricht
	Data[1]=0x08;//Id der Nachricht
	Data[2]=6;//l�nge der folgenden Nachricht
	Data[3]=0;//l�nge
	Data[4]=periode&0xff;
	Data[5]=(periode>>8)&0xff;
	Data[6]=navRate;
	Data[7]=0;
	Data[8]=TimeRef;
	Data[9]=0;
	
	
	send_config(Data,10);
	
}