﻿using Helper;
using System;
using System.Collections.Generic;

namespace ComMang
{
    public static class CmdMang
    {
        //generiert aus einem Byte Array zwei Prüfziffern (wurde nicht implementiert)
        public static byte[] PrfSumCalc(byte[] array)
        {
            byte a = 0;
            byte b = 0;

            //byte[] array = ASCIIEncoding.ASCII.GetBytes(val);

            int length = array.Length;

            for (int i = 0; i < length; i++)
            {
                a += array[i];
                b += a;
            }

            byte[] asdf = { a, b };

            return asdf;
            //return ASCIIEncoding.ASCII.GetString(asdf);
        }

        //Überprüft, ob die gelesene Nachrichten zur gesendeten Nachricht passen
        public static bool AnswerIsValid(OutMsg outmsg, InMsgList inlist)
        {
            if (outmsg is object && inlist is object)
            {
                int count = inlist.Count;
                if (outmsg.IsRequest)
                {
                    switch (outmsg.Requtype)
                    {
                        case RequType.WhoU:
                            if (count == 2)
                            {
                                if (inlist[0].Type == InType.ID && inlist[1].Type == InType.Konfig) return true;
                            }
                            break;
                        case RequType.SendAll:
                            if (count >= 2)
                            {
                                int header = 0;
                                int body = 0;

                                for (int i = 0; i < count; i++)
                                {
                                    if (inlist[i].Type == InType.Header) header++;
                                    else if (inlist[i].Type == InType.SamplePoint) body++;
                                    else return false;
                                }

                                if (header != 0 && body != 0 && header <= body)
                                {
                                    return true;
                                }
                            }
                            break;
                        //case RequType.Konfigs:
                        //    break;
                        //Not Implemented!!!!!!!!!
                        default:
                            return false;
                    }
                }
                else
                {
                    if (count == 1)
                    {
                        if (inlist[0].Type == InType.ACK) return true;
                    }
                }
            }
            return false;
        }
    }

    /// <summary>
    /// Hilft die einkommenden Nachrichten bequemer zusammen zu fassen
    /// </summary>
    public class InMsgList : List<InMsg> { }

    //Bildet eine einkommende Nachricht ab
    public class InMsg
    {
        public bool ACK { get; set; }
        public RideDataSample SamplePoint { get; set; }
        public int ID { get; set; }
        public Konfigurations Konfig { get; set; }
        public DateTime HeaderTime { get; set; }
        public int HeaderSR { get; set; }
        public InType Type { get; set; }

        /// <summary>
        /// Generiert aus einem Eingabestring das passende Abbild einer eingetroffenen Nachricht
        /// </summary>
        /// <param name="msg">Eine ganze Zeile aus der USB Kommunikation</param>
        public InMsg(string msg)
        {
            string[] tockens = msg.Split('=');

            switch (tockens[0])
            {
                case "ACK":
                    Type = InType.ACK;
                    ACK = true;
                    break;
                case "NACK":
                    Type = InType.ACK;
                    ACK = false;
                    break;
                case "ID":
                    Type = InType.ID;
                    ID = int.Parse(tockens[1]);
                    break;
                case "Konfig":
                    Type = InType.Konfig;
                    Konfig = new Konfigurations(tockens[1]);
                    break;
                case "Data":
                    Type = InType.SamplePoint;
                    SamplePoint = new RideDataSample(tockens[1]);
                    break;
                case "H":
                    Type = InType.Header;
                    string[] head = tockens[1].Split(';');
                    HeaderTime = DateTime.Parse(head[0]);
                    HeaderSR = int.Parse(head[1]);
                    break;
                default:
                    break;
            }
        }
    }

    /// <summary>
    /// Bildet eine Ausgehende Nachricht ab
    /// </summary>
    public class OutMsg
    {
        public bool IsRequest { get; set; }
        public string Value { get; set; }
        public ConfType Conftype { get; set; }
        public RequType Requtype { get; set; }

        /// <summary>
        /// Konstrucktor um eine Konfignachricht zu erstellen
        /// </summary>
        /// <param name="conftype">Typ</param>
        /// <param name="val">Wert (je nach Befehl optional)</param>
        public OutMsg(ConfType conftype, string val = "")
        {
            IsRequest = false;
            Conftype = conftype;
            Value = val;
        }

        /// <summary>
        /// Konstrucktor um eine Requestnachricht zu erstellen
        /// </summary>
        /// <param name="requtype">Type</param>
        public OutMsg(RequType requtype)
        {
            IsRequest = true;
            Requtype = requtype;
        }

        /// <summary>
        /// Generiert aus sich den jeweiligen Befehl
        /// </summary>
        /// <returns>Gibt den Befehl als sendbaren string zurück</returns>
        public override string ToString()
        {
            string cmd = "";

            if (IsRequest)
            {
                cmd += "?";

                switch (Requtype)
                {
                    case RequType.WhoU:
                        cmd += "U";
                        break;
                    case RequType.SendAll:
                        cmd += "All";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                cmd += "!";

                switch (Conftype)
                {
                    case ConfType.Name:
                        cmd += $"Name={Value}";
                        break;
                    case ConfType.Samplerate:
                        cmd += $"SR={Value}";
                        break;
                    case ConfType.Loeschen:
                        cmd += "CLEAR";
                        break;
                    default:
                        break;
                }
            }

            return cmd;
        }
    }

    public enum InType
    {
        ID,
        SamplePoint,
        ACK,
        Konfig,
        Header
    }

    public enum ConfType
    {
        Name,
        Samplerate,
        Loeschen,
    }

    public enum RequType
    {
        WhoU,
        SendAll
    }
}
