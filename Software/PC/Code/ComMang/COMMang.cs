﻿using Helper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;

namespace ComMang
{
    public static class COMMang
    {
        private static SerialPort _serialport;
        private static bool _continue;
        private static ObservableCollection<string> allmsgs = new ObservableCollection<string>();
        private const int RWTOTestGL = 50;
        private const int RWTONormal = 500;
        private const int RWTODebug = -1;
        private static int RWTO;
        private static bool timeout;

        public static bool Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }


        public static ObservableCollection<string> AllMsgs
        {
            get { return allmsgs; }
            private set { allmsgs = value; }
        }

        /// <summary>
        /// Versucht einen Port mit den von uns fixierten Einstellungen zu öffnen
        /// </summary>
        /// <param name="portname">Name des zu öffnenden Ports (z.B.: COM3)</param>
        /// <returns>Erfolg/Misserfolg</returns>
        public static bool DefaultInit(string portname)
        {
            return InitCon(portname, 110000, Parity.None, 8, StopBits.One, Handshake.None, -1, -1);
        }

        /// <summary>
        /// Wie Deafultinit nur das jeder Parameter der Porteinstellungen gesetzt werden muss
        /// </summary>
        /// <returns>Erfolg</returns>
        public static bool InitCon(string portname, int baudrate, Parity parity, int databits, StopBits stopbits, Handshake handshake, int readtimeout, int writetimeout)
        {
            _serialport = new SerialPort(portname, baudrate, parity, databits, stopbits);
            _serialport.Handshake = handshake;
            _serialport.ReadTimeout = readtimeout;
            _serialport.WriteTimeout = writetimeout;
            _serialport.NewLine = "\r\n";

            try
            {
                _serialport.Open();
                return true;
            }
            catch (Exception)
            {
                //MessageBox.Show(exp.Message);
                return false;
            }
        }

        /// <summary>
        /// Sendet eine Zeile über die COM-Schnittstelle
        /// </summary>
        /// <param name="msg">Zeile welche gesendet werden soll</param>
        /// <returns>Erfolg/Misserfolg</returns>
        private static bool Write(string msg)
        {
            //AllMsgs.Add(msg);
            try
            {
                _serialport.WriteLine(msg);
            }
            catch (TimeoutException)
            {
                Timeout = true;
                return false;
            }
            catch (Exception)
            {
                //MessageBox.Show(exp.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// schließt den geöffneten Port
        /// </summary>
        public static void ClosePort()
        {
            _serialport.Close();
        }

        /// <summary>
        /// schließt den Port und davor die GL-Verbindung
        /// </summary>
        public static void Close()
        {
            _serialport.WriteTimeout = 100;
            Write("##");
            _serialport.Close();
            _serialport.WriteTimeout = -1;
        }

        /// <summary>
        /// liest so lange Zeilenweise an der COM-Schnittstelle, bis ein $ auftritt
        /// </summary>
        /// <returns>Liste aller gelesenen Nachrichten</returns>
        private static InMsgList Read()
        {
            string message = "";
            InMsgList list = new InMsgList();
            while (_continue)
            {
                try
                {
                    message = _serialport.ReadLine();
                    //AllMsgs.Add(message);
                    if (message.Contains("$"))
                    {
                        _continue = false;
                        break;
                    }
                    list.Add(new InMsg(message));
                    _serialport.ReadTimeout = -1;
                    _serialport.ReadTimeout = RWTO;
                }
                catch (TimeoutException)
                {
                    Timeout = true;
                    return null;
                }
                catch (Exception)
                {
                    //MessageBox.Show(exp.Message);
                    return null;
                }

            }

            return list;
        }

        /// <summary>
        /// Vereinfachung für das ausführen von Befehlen (senden dann bis $ lesen [Timeout, etc.])
        /// </summary>
        /// <param name="msg">zu sendende Nachricht</param>
        /// <param name="TimeoutType">Je nach Art der Kommunikation kann aus einer von drei Zeitspannen ausgewählt werden: 0 = 20ms, 1 = 500ms, 2 = 1s</param>
        /// <returns>gelesene InMsgList</returns>
        public static InMsgList SendCommandReadAnswer(OutMsg msg, int TimeoutType = 1)
        {
            switch (TimeoutType)
            {
                case 0:
                    {
                        RWTO = RWTOTestGL;
                        break;
                    }
                case 1:
                    {
                        RWTO = RWTONormal;
                        break;
                    }
                case 2:
                    {
                        RWTO = RWTODebug;
                        break;
                    }
                default:
                    throw new Exception("Falscher Timeouttype!");
            }

            _serialport.ReadTimeout = RWTO;
            _serialport.WriteTimeout = RWTO;

            InMsgList list;
            Timeout = false;

            if (Write(msg.ToString()))
            {
                _continue = true;
                list = Read();

                _serialport.ReadTimeout = -1;
                _serialport.WriteTimeout = -1;

                return list;
            }

            return null;
        }

        /// <summary>
        /// Sucht alle verbundenen COM-Geräte (Portname)
        /// </summary>
        /// <returns>Liste dieser Namen</returns>
        public static ObservableCollection<string> getAvailablePorts()
        {
            ObservableCollection<string> list = new ObservableCollection<string>();
            try { foreach (string s in SerialPort.GetPortNames()) { list.Add(s); } }
            catch (Exception) { }// MessageBox.Show(exp.Message); }
            return list;
        }
    }

    public static class COMHelper
    {
        public static bool debug = false;
        /// <summary>
        /// versucht die neuen Konfigurationen umzusetzten
        /// </summary>
        /// <param name="konfignew">neue Konfigurationen</param>
        /// <param name="konfigold">bisherige Konfigurationen</param>
        /// <returns>Konfigurationen nach dem Vorgang</returns>
        public static Konfigurations trysetnewKonfigs(Konfigurations konfignew, Konfigurations konfigold)
        {
            string name = "";
            int samplerate = 1;

            if (konfignew.Name != konfigold.Name)
            {
                if (trysetoneKonfig(ConfType.Name, konfignew.Name))
                {
                    name = konfignew.Name;
                }
                else
                {
                    name = konfigold.Name;
                }
            }
            else
            {
                name = konfigold.Name;
            }

            if (konfignew.SampleRate != konfigold.SampleRate)
            {
                string val = konfignew.SampleRate.ToString();
                if (trysetoneKonfig(ConfType.Samplerate, val))
                {
                    samplerate = konfignew.SampleRate;
                }
                else
                {
                    samplerate = konfigold.SampleRate;
                }
            }
            else
            {
                samplerate = konfigold.SampleRate;
            }

            Konfigurations konfig = new Konfigurations(name, samplerate);

            return konfig;
        }

        /// <summary>
        /// Versucht eine Konfiguration umzusetzten
        /// </summary>
        /// <param name="type">Parameter welcher verändert wird</param>
        /// <param name="value">optionaler Wert</param>
        /// <returns>Erfolg/Misserfolg</returns>
        public static bool trysetoneKonfig(ConfType type, string value = "")
        {
            try
            {
                OutMsg msg = new OutMsg(type, value);
                InMsgList list = debug ? COMMang.SendCommandReadAnswer(msg, 2) : COMMang.SendCommandReadAnswer(msg);

                if (list is null) throw new Exception("Keine Antwort");
                if (!CmdMang.AnswerIsValid(msg, list)) throw new Exception("Ungültige Antwort");

                return list[0].ACK;
            }
            catch (Exception)
            {
                //MessageBox.Show("Beim Ausführen einer Konfiguration ist ein Fehler aufgetreten: " + exp.Message);
                return false;
            }
        }

        public static List<bool> GLforsure(ObservableCollection<string> ports)
        {
            List<bool> outlist = new List<bool>();

            int j = ports.Count;
            for (int i = 0; i < j; i++)
            {
                try
                {
                    var konf = InitCon(ports[i], 0);
                    COMMang.ClosePort();
                    if (konf is null) outlist.Add(false);
                    else outlist.Add(true);
                }
                catch (Exception)
                {
                    outlist.Add(false);
                }
            }

            return outlist;
        }

        /// <summary>
        /// Versucht den Speicher des GL auszulesen
        /// </summary>
        /// <returns>Liste von Aufzeichnungen</returns>
        public static List<RideDataList> ReadAll()
        {
            try
            {
                OutMsg msg = new OutMsg(RequType.SendAll);
                InMsgList inlist = debug ? COMMang.SendCommandReadAnswer(msg, 2) : COMMang.SendCommandReadAnswer(msg);

                if (inlist is null) throw new Exception("Keine Antwort");
                if (!CmdMang.AnswerIsValid(msg, inlist)) throw new Exception("Ungültige Antwort");

                return RideDataListFromInMsgList(inlist);
            }
            catch (Exception)
            {
                //MessageBox.Show("Beim Lesen der Daten gab es ein Problem: " + exp.Message);
                return null;
            }
        }

        /// <summary>
        /// Konvertiert eine InMsgList zu einer Liste von Aufzeichnungen
        /// </summary>
        /// <param name="inlist">gelesene InMsgList</param>
        /// <returns>List of RideDataList</returns>
        private static List<RideDataList> RideDataListFromInMsgList(InMsgList inlist)
        {
            List<RideDataList> outlist = new List<RideDataList>();
            int ridecount = -1;

            foreach (InMsg item in inlist)
            {
                if (item.Type == InType.Header)
                {
                    ridecount++;
                    outlist.Add(new RideDataList());
                    outlist[ridecount].SampleRate = item.HeaderSR;
                    outlist[ridecount].StartTime = item.HeaderTime;
                }
                else if (item.Type == InType.SamplePoint)
                {
                    outlist[ridecount].Add(item.SamplePoint);
                }
                else return null;
            }

            return outlist;
        }

        /// <summary>
        /// versucht den Speicher zu löschen
        /// </summary>
        /// <returns>Erfolg/Misserfolg</returns>
        public static bool Clear()
        {
            return trysetoneKonfig(ConfType.Loeschen);
        }

        private static int id;

        public static int ID
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// versucht eine COM und GL Verbindung herzustellen
        /// </summary>
        /// <param name="portname">Name des zu verbindendes Gerät (z.B.: COM3)</param>
        /// <param name="TOType">Je nach Art der Kommunikation kann aus einer von drei Zeitspannen ausgewählt werden: 0 = 20ms, 1 = 500ms, 2 = 1s</param>
        /// <returns>Konfiguration des Gerätes (ID wird auf Property ID geschrieben)</returns>
        public static Konfigurations InitCon(string portname, int TOType = 1)
        {
            if (!string.IsNullOrWhiteSpace(portname) && !string.IsNullOrEmpty(portname))
            {
                try
                {
                    if (COMMang.DefaultInit(portname))
                    {
                        InMsgList list = new InMsgList();
                        OutMsg msg = new OutMsg(RequType.WhoU);
                        list = debug ? COMMang.SendCommandReadAnswer(msg, 2) : COMMang.SendCommandReadAnswer(msg, TOType);

                        if (list is null) throw new Exception("Keine Antwort");
                        if (!CmdMang.AnswerIsValid(msg, list)) throw new Exception("Ungültige Antwort");

                        ID = list[0].ID;
                        if (ID == 0) throw new Exception("Keine ID");

                        return list[1].Konfig;
                    }
                }
                catch (Exception)
                {
                    COMMang.ClosePort();
                    //MessageBox.Show("Fataler fehler beim Verbindungsaufbau, möglicher Geo-Logger konnte nicht identifiziert werden: " + exp.Message);
                }
            }

            ID = 0;
            return null;
        }
    }
}
