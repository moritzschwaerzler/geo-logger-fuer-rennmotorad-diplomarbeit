﻿using ComMang;
using Helper;
using SaveLoadMang;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace MVVM
{
    public class mvvm : INotifyPropertyChanged, IDisposable
    {
        private readonly Brush ConOkBrush = Brushes.ForestGreen;
        private readonly Brush ConNotOkBrush = Brushes.OrangeRed;
        private readonly Brush NothingBrush = Brushes.LightSkyBlue;

        //fields
        private string selectedcom;
        private ObservableCollection<string> readycoms;
        private Brush statuscolor;
        private string islabel;
        private Visibility devicedetailsvisibility;
        private string name;
        private int samplerate;
        private bool isconnected;
        private string btconnectcontent;
        private Konfigurations konfig;
        private Konfigurations konfignew;
        private ObservableCollection<string> usbmsgs;
        private int sampleratenew;
        private string namenew;
        private string output;
        private Visibility btreadvisi;
        private bool mainenable;
        private Visibility sandvisi;

        public Visibility SandVisi
        {
            get { return sandvisi; }
            set { sandvisi = value; OnPropChanged("SandVisi"); }
        }


        public bool MainEnable
        {
            get { return mainenable; }
            set { mainenable = value; OnPropChanged("MainEnable"); }
        }

        Task WorkTask;

        private Cursor cursor;

        private bool coboenable;

        private Brush statusselected;

        public Brush StatusSelected
        {
            get { return statusselected; }
            set { statusselected = value; OnPropChanged("StatusSelected"); }
        }


        public bool CoBoEnable
        {
            get { return coboenable; }
            set { coboenable = value; OnPropChanged("CoBoEnable"); }
        }


        public Cursor CursorBind
        {
            get { return cursor; }
            set { cursor = value; OnPropChanged("CursorBind"); }
        }

        private void InitWork()
        {
            Output = "Antwort des Geo-Logger wird abgewartet. Dies kann einige Sekunden daruern!";
            MainEnable = false;
            SandVisi = Visibility.Visible;
            CursorBind = Cursors.Wait;
        }

        public bool IsDebug { get; set; } = false;

        public void NoDebug()
        {
            COMHelper.debug = false;
            IsDebug = false;
            Output = "Debug OFF!";
        }

        public void Debug()
        {
            IsDebug = true;
            COMHelper.debug = true;
            Output = "Debug ON! Achtung kein Timeout";
        }

        private void FinishedWork()
        {
            MainEnable = true;
            SandVisi = Visibility.Collapsed;
            CursorBind = Cursors.Arrow;
            WorkTask.Dispose();
            WorkTask = null;
        }


        //events
        public event PropertyChangedEventHandler PropertyChanged;

        #region methoden konstrucktor eventhandler usw
        public void OnPropChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public mvvm()
        {
            //Defaultwerte setzen
            MainEnable = true;
            SandVisi = Visibility.Collapsed;
            BTConnectContent = "Verbinden";
            DeviceDetailsVisibility = Visibility.Hidden;
            StatusColor = ConNotOkBrush;
            BTReadFileVisibility = Visibility.Collapsed;
            CursorBind = Cursors.Arrow;
            CoBoEnable = true;
            COMMang.AllMsgs.CollectionChanged += AllMsgs_CollectionChanged;
        }

        private void AllMsgs_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            USBMsgs = COMMang.AllMsgs;
        }

        /// <summary>
        /// zuständig für den Verbindungsaufbau und Abbruch
        /// </summary>
        public void USBConnectDisconnect()
        {
            InitWork();

            WorkTask = new Task(USBConnectDisconnectSub);
            WorkTask.Start();
            WorkTask.ContinueWith((a) => { FinishedWork(); });
        }

        private void USBConnectDisconnectSub()
        {
            CoBoEnable = false;
            if (!IsConnected)
            {
                if (string.IsNullOrEmpty(SelectedCom) || string.IsNullOrWhiteSpace(SelectedCom))
                {
                    Output = "kein Gerät zum Verbinden ausgewählt";
                    DeviceDetailsVisibility = Visibility.Hidden;
                    StatusColor = ConNotOkBrush;
                    BTConnectContent = "Verbinden";
                    IsConnected = false;
                    IDLabel = "";
                    CoBoEnable = true;
                    return;
                }

                Konfigs = COMHelper.InitCon(SelectedCom);

                if (CheckTimeout())
                {
                    CoBoEnable = true;
                    return;
                }

                if (Konfigs is null)
                {
                    DeviceDetailsVisibility = Visibility.Hidden;
                    StatusColor = ConNotOkBrush;
                    Output = "Verbinden mit dem Gerät ist fehlgeschlagen";
                    BTConnectContent = "Verbinden";
                    IsConnected = false;
                    IDLabel = "";
                    CoBoEnable = true;
                }
                else
                {
                    KonfigsNew = Konfigs;
                    setNewKonfigProps();
                    DeviceDetailsVisibility = Visibility.Visible;
                    StatusColor = ConOkBrush;
                    BTConnectContent = "Trennen";
                    Output = "Erfolgreich verbunden";
                    IsConnected = true;
                    IDLabel = $"Verbunden mit {Konfigs.Name} mit der ID:{COMHelper.ID}";
                    CoBoEnable = false;
                }
            }
            else
            {
                COMMang.Close();
                IsConnected = false;
                DeviceDetailsVisibility = Visibility.Hidden;
                StatusColor = ConNotOkBrush;
                BTConnectContent = "Verbinden";
                Output = "Verbindung getrennt";
                IDLabel = "";
                CoBoEnable = true;
            }
        }

        /// <summary>
        /// versucht den Speicher des GL zu lesen und jeden davon lokal abzuspeichern
        /// </summary>
        public void ReadAndSave()
        {
            InitWork();
            WorkTask = new Task(ReadAndSaveSub);
            WorkTask.Start();
            WorkTask.ContinueWith((a) => { FinishedWork(); });
        }

        private void ReadAndSaveSub()
        {
            var list = COMHelper.ReadAll();
            if (CheckTimeout()) return;

            if (list != null)
            {
                int von = 0;
                int ist = 0;
                foreach (RideDataList item in list)
                {
                    von++;
                    if (CsvRw.SaveFile(item)) ist++;
                }

                Output = $"Es wurden {von} Datensätze gelesen, von denen {ist} gespeichert wurden";
            }
            else Output = "Es konnten keine Daten gelesen werden";
        }

        /// <summary>
        /// aktualisiert die verbundenen COM-Geräte
        /// </summary>
        public void RefreshPorts()
        {
            ReadyComs = COMMang.getAvailablePorts();

            if (ReadyComs.Count > 0) SelectedIndex = 0;
            else StatusSelected = NothingBrush;

            Output = $"mögliche verfügbare Geräte wurden aktualisiert, {ReadyComs.Count} wurden gefunden";

            GLArray = COMHelper.GLforsure(ReadyComs);

            SelectedIndex = -1;
            SelectedIndex = 0;
        }

        /// <summary>
        /// versucht den Flash zu löschen
        /// </summary>
        public void DeleteOnLogger()
        {
            InitWork();
            WorkTask = new Task(DeleteOnLoggerSub);
            WorkTask.Start();
            WorkTask.ContinueWith((a) => { FinishedWork(); });
        }

        private void DeleteOnLoggerSub()
        {
            if (COMHelper.Clear()) Output = "Speicher erfolgreich gelöscht";
            else Output = "Speicher konnte nicht gelöscht werden";

            if (CheckTimeout()) return;
        }

        /// <summary>
        /// Lesen eines Files; öffnen eines Fensters um die Datén anzuzeigen (für Debugging)
        /// </summary>
        public void ReadFile()
        {
            var list = CsvRw.LoadFile();
            if (list is object)
            {
                Window win = new Window();
                StackPanel stack = new StackPanel();
                Label lbl = new Label();
                lbl.Content = $"Aufzeichung vom: {list.StartTime:dd.MM.yyyy}, mit einer Samplerate von {list.SampleRate}";
                var box = new ListBox();
                box.Height = 400;
                box.Margin = new Thickness(10.0);
                box.ItemsSource = list;
                stack.Children.Add(lbl);
                stack.Children.Add(box);
                win.Content = stack;
                Output = "Daten aus Datei wurden gelesen";
                win.ShowDialog();
            }
            else Output = "keine Aufzeichungsdaten gelesen!";
        }

        /// <summary>
        /// versucht die im Konfigwindow eingestellten Konfigurationen auf den verbundenen GL zu übertragen
        /// </summary>
        public void SetNewKonfigs()
        {
            InitWork();
            WorkTask = new Task(SetNewKonfigsSub);
            WorkTask.Start();
            WorkTask.ContinueWith((a) => { FinishedWork(); });
        }

        private void SetNewKonfigsSub()
        {
            GenNewKonfigs();

            Konfigurations konfignow = COMHelper.trysetnewKonfigs(KonfigsNew, Konfigs);
            if (CheckTimeout()) return;

            int notdone = Konfigurations.differentKonfigs(KonfigsNew, konfignow);
            int von = Konfigurations.differentKonfigs(Konfigs, KonfigsNew);

            if (von == 0) Output = "Keine neuen Konfigurationen";
            else if (notdone == 0) Output = $"Alle Konfigurationen ({von}) umgesetzt";
            else Output = $"{von - notdone} von {von} Konfigurationen konnten umgesetzt werden umgesetzt";

            Konfigs = konfignow;
            KonfigsNew = Konfigs;

            setNewKonfigProps();

            IDLabel = $"Verbunden mit {Konfigs.Name} mit der ID:{COMHelper.ID}";
        }

        /// <summary>
        /// Hilfsmethode um aus den eingestellten Parametern ein neues Konfig Objekt zu generieren
        /// </summary>
        public void GenNewKonfigs()
        {
            KonfigsNew = new Konfigurations(NameNew, SampleRateNew);
        }

        /// <summary>
        /// Hilfsmethode um aus einer Konfig die Props zum Binding zu füllen
        /// </summary>
        public void setNewKonfigProps()
        {
            SampleRateNew = KonfigsNew.SampleRate;
            NameNew = KonfigsNew.Name;
        }

        /// <summary>
        /// Hilfsmethode, welche prüft ob ein Timeout aufgetreten ist + UI + Verbindung trennen wenn Timeout
        /// </summary>
        /// <returns>true bei Timeout</returns>
        private bool CheckTimeout()
        {
            if (COMMang.Timeout)
            {
                COMMang.ClosePort();
                DeviceDetailsVisibility = Visibility.Hidden;
                StatusColor = ConNotOkBrush;
                Output = "Verbindung mit dem Gerät ist fehlgeschlagen, etwas hat zu lange gedauert";
                BTConnectContent = "Verbinden";
                IsConnected = false;
                IDLabel = "";
                return true;
            }

            return false;
        }

        /// <summary>
        /// Aktionen welche ausgeführt werden müssen, wenn die Anwendung geschlossen wird
        /// </summary>
        public void Dispose()
        {
            if (IsConnected) COMMang.Close();
        }
        #endregion


        #region Props
        public Visibility BTReadFileVisibility
        {
            get { return btreadvisi; }
            set { btreadvisi = value; OnPropChanged("BTReadFileVisibility"); }
        }

        public string Output
        {
            get { return output; }
            set { output = value; OnPropChanged("Output"); }
        }

        public string NameNew
        {
            get { return namenew; }
            set { namenew = value; OnPropChanged("NameNew"); }
        }

        public int SampleRateNew
        {
            get { return sampleratenew; }
            set { sampleratenew = value; OnPropChanged("SampleRateNew"); }
        }

        public ObservableCollection<string> USBMsgs
        {
            get { return usbmsgs; }
            set { usbmsgs = value; OnPropChanged("USBMsgs"); }
        }

        public Konfigurations KonfigsNew
        {
            get { return konfignew; }
            set { konfignew = value; OnPropChanged("KonfigsNew"); setNewKonfigProps(); }
        }

        public Konfigurations Konfigs
        {
            get { return konfig; }
            set { konfig = value; OnPropChanged("Konfigs"); }
        }

        public string BTConnectContent
        {
            get { return btconnectcontent; }
            set { btconnectcontent = value; OnPropChanged("BTConnectContent"); }
        }

        public bool IsConnected
        {
            get { return isconnected; }
            set { isconnected = value; }
        }

        public int SampleRate
        {
            get { return samplerate; }
            set { samplerate = value; OnPropChanged("SampleRate"); }
        }

        public string Name
        {
            get { return name; }
            set { name = value; OnPropChanged("Name"); }
        }

        public Visibility DeviceDetailsVisibility
        {
            get { return devicedetailsvisibility; }
            set { devicedetailsvisibility = value; OnPropChanged("DeviceDetailsVisibility"); }
        }

        public string IDLabel
        {
            get { return islabel; }
            set { islabel = value; OnPropChanged("IDLabel"); }
        }

        public Brush StatusColor
        {
            get { return statuscolor; }
            set { statuscolor = value; OnPropChanged("StatusColor"); }
        }

        public ObservableCollection<string> ReadyComs
        {
            get { return readycoms; }
            set { readycoms = value; OnPropChanged("ReadyComs"); }
        }

        public string SelectedCom
        {
            get { return selectedcom; }
            set { selectedcom = value; OnPropChanged("SelectedCom"); RefreshSelectedColor(); }
        }

        private int selectedindex;

        public int SelectedIndex
        {
            get { return selectedindex; }
            set { selectedindex = value; OnPropChanged("SelectedIndex"); }
        }

        private List<bool> glarray;

        public List<bool> GLArray
        {
            get { return glarray; }
            set { glarray = value; }
        }


        #endregion


        private void RefreshSelectedColor()
        {
            if (SelectedIndex > -1 && SelectedIndex < GLArray.Count) StatusSelected = GLArray[SelectedIndex] ? ConOkBrush : ConNotOkBrush;
        }
    }
}
