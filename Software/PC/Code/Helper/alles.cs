﻿using System;
using System.Collections.Generic;

namespace Helper
{
    /// <summary>
    /// Mit dieser Klasse kann ein Aufzeichnungspunkt abgebildet werden.
    /// </summary>
    public class RideDataSample
    {
        public string Laenge { get; set; }
        public string Breite { get; set; }
        public string Hoehe { get; set; }
        public char NordSued { get; set; }
        public char OstWest { get; set; }
        public TimeSpan TimeStemp { get; set; }

        /// <summary>
        /// Füllt Props über string
        /// </summary>
        /// <param name="val">Laut Geo-Logger-Protokoll (GLP)</param>
        public RideDataSample(string val)
        {
            string[] tockens = val.Split(';');
            Laenge = tockens[0];
            Breite = tockens[1];
            Hoehe = tockens[2];
            NordSued = Convert.ToChar(tockens[3]);
            OstWest = Convert.ToChar(tockens[4]);
            TimeStemp = TimeSpan.Parse(tockens[5]);
        }

        public RideDataSample(string laenge, string breite, string hoehe, char nordsued, char ostwest, TimeSpan time)
        {
            Laenge = laenge;
            Breite = breite;
            NordSued = nordsued;
            OstWest = ostwest;
            TimeStemp = time;
            Hoehe = hoehe;
        }

        /// <summary>
        /// Serialisert laut GLP
        /// </summary>
        public override string ToString()
        {
            return $"{Laenge};{Breite};{Hoehe};{NordSued};{OstWest};{TimeStemp}";
        }
    }

    /// <summary>
    /// Stellt eine Aufzeichnung dar: Header und Data
    /// </summary>
    public class RideDataList : List<RideDataSample>
    {
        public DateTime StartTime { get; set; }
        public int SampleRate { get; set; }
    }

    /// <summary>
    /// Stellt eine Konfiguration des GL dar
    /// </summary>
    public class Konfigurations
    {
        /// <returns>Anzahl der unterschiedlichen Konfigurationen</returns>
        public static int differentKonfigs(Konfigurations konfig1, Konfigurations konfig2)
        {
            int a = 0;

            if (konfig1.Name != konfig2.Name) a++;

            if (konfig1.SampleRate != konfig2.SampleRate) a++;

            return a;
        }
        public string Name { get; set; }
        public int SampleRate { get; set; }

        /// <summary>
        /// Füllt Props über string
        /// </summary>
        /// <param name="val">laut GLP</param>
        public Konfigurations(string val)
        {
            string[] tockens = val.Split(';');
            Name = tockens[0];
            SampleRate = int.Parse(tockens[1]);
        }

        public Konfigurations(string name, int samplerate)
        {
            Name = name;
            SampleRate = samplerate;
        }
    }
}