﻿using MVVM;
using System.Windows;

namespace Geo_Logger_v2
{
    /// <summary>
    /// Interaktionslogik für usb_dev_win.xaml
    /// </summary>
    public partial class usb_dev_win : Window
    {
        mvvm mvvm;
        public usb_dev_win(ref mvvm mvvm)
        {
            InitializeComponent();

            this.mvvm = mvvm;

            DataContext = this.mvvm;

            this.mvvm.BTReadFileVisibility = Visibility.Visible;
        }
    }
}
