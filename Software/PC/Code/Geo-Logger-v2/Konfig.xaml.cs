﻿using MVVM;
using System.Text.RegularExpressions;
using System.Windows;

namespace Geo_Logger_v2
{
    /// <summary>
    /// Interaktionslogik für Konfig.xaml
    /// </summary>
    public partial class Konfig : Window
    {
        mvvm mvvm;
        bool winloaded = false;

        //referenziert auf das gleiche mvvm wie unser MainWin
        //"Defaultwerte" setzten; Datakontext
        public Konfig(ref mvvm mvvm)
        {
            InitializeComponent();
            winloaded = true;
            this.mvvm = mvvm;
            DataContext = this.mvvm;
            slsamplerate.Value = this.mvvm.SampleRateNew;
        }

        //Überprüft den Namen und schließt dann das Fenster mit einem positiven Dialogresult
        private void tbsave_Click(object sender, RoutedEventArgs e)
        {
            if (CheckName())
            {
                DialogResult = true;
                Close();
            }
            else
            {
                MessageBox.Show("Bitte den Aliasnamen des Logger richtig eingeben");
            }
        }

        /// <summary>
        /// eingegebener Name wird auf richtigkeit überprüft (Alphanumerische Folge von 1 bis 10 Zeichen)
        /// </summary>
        /// <returns>true wenn Name valide ist</returns>
        private bool CheckName()
        {
            string name = mvvm.NameNew;
            if (name.Length > 10 || name.Length < 1) return false;

            return Regex.IsMatch(name, @"^[a-zA-Z0-9]*$", RegexOptions.Compiled);
        }

        //Fenster schließen ohne zu speichern
        private void btstop_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        //Bei Änderung des Sliders muss der Wert "manuell" auf das Property gesetzt werden da der Wert zerst auf int konvertiert werden muss
        private void slsamplerate_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!winloaded) return;
            mvvm.SampleRateNew = (int)slsamplerate.Value;
        }
    }
}
