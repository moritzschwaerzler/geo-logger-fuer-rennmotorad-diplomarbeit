﻿using MVVM;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Geo_Logger_v2
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        mvvm mvvm;
        usb_dev_win devwin;
        Window helpwin;

        //mvvm Objekt erzeugen und den Datakontext darauf setzten
        //verfügbare Geräte aktualisieren
        public MainWindow()
        {
            InitializeComponent();

            mvvm = new mvvm();

            DataContext = mvvm;

            mvvm.RefreshPorts();
        }

        //gespeicherte Datei öffnen
        private void btopen_Click(object sender, RoutedEventArgs e)
        {
            mvvm.ReadFile();
        }

        //Verbindung trennen/herstellen
        private void btConnect_Click(object sender, RoutedEventArgs e)
        {
            mvvm.USBConnectDisconnect();
        }

        //COM-Geräte akutalisieren
        private void RefreshPorts_Click(object sender, RoutedEventArgs e)
        {
            mvvm.RefreshPorts();
        }

        //GL auslesen und Daten abspeichern
        private void btread_Click(object sender, RoutedEventArgs e)
        {
            mvvm.ReadAndSave();
        }

        //KonfigWin als Dialog öffnen, bei positivem Dialogresult werden die neuen Konfigurationen umgesetzt
        private void btkonfig_Click(object sender, RoutedEventArgs e)
        {
            Konfig win = new Konfig(ref mvvm);
            if (win.ShowDialog() == true)
            {
                mvvm.SetNewKonfigs();
            }
        }

        //Speichern auf GL löschen
        private void btdelete_Click(object sender, RoutedEventArgs e)
        {
            mvvm.DeleteOnLogger();
        }

        //Develoment Window (zeigt alle Nachrichten, welche über die COM-Schnittstelle gesendet oder empfangen wurden)
        private void btConnect_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (devwin == null && mvvm.IsDebug)
            {
                devwin = new usb_dev_win(ref mvvm);
                devwin.Show();
                devwin.Closed += Devwin_Closed;
            }
        }
        private void Devwin_Closed(object sender, System.EventArgs e)
        {
            devwin = null;
        }

        //HilfsWindow (auch für die Entwicklung als Hilfestellung zu den nötigen Befehlen
        private void RefreshPorts_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (helpwin == null && mvvm.IsDebug)
            {
                helpwin = new Window();
                TextBox tb = new TextBox();
                tb.MaxLines = 20;
                tb.Text = "ID=123456\r\nKonfig=Hansi1;5\r\nH=17.02.2021;5\r\nData=2356;5689;8012;N;O;12:13:43.1234567\r\nData=2356;5689;8012;N;O;12:13:43.1234567\r\nData=2356;5689;8012;N;O;12:13:43.1234567\r\nData=2356;5689;8012;N;O;12:13:43.1234567\r\nData=2356;5689;8012;N;O;12:13:43.1234567\r\nH=17.02.2021;5\r\nData=2356;5689;8012;N;O;12:13:43.1234567\r\nData=2356;5689;8012;N;O;12:13:43.1234567\r\nData=2356;5689;8012;N;O;12:13:43.1234567\r\nData=2356;5689;8012;N;O;12:13:43.1234567\r\n\r\n\r\n\r\n\r\n\r\n\r\n";
                helpwin.Content = tb;
                helpwin.Show();
                helpwin.Closed += Helpwin_Closed;
            }
        }
        private void Helpwin_Closed(object sender, System.EventArgs e)
        {
            helpwin = null;
        }

        //Bei schließen der Anwendung alle Subfenster schließen wenn geöffnet und mvvm Disposen
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (devwin != null && devwin.IsVisible) devwin.Close();

            if (helpwin != null && helpwin.IsVisible) helpwin.Close();

            mvvm.Dispose();
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.PageUp:
                    mvvm.Debug();
                    break;
                case Key.PageDown:
                    if (devwin != null && devwin.IsVisible) devwin.Close();
                    if (helpwin != null && helpwin.IsVisible) helpwin.Close();
                    mvvm.NoDebug();
                    break;
            }
        }
    }
}
