﻿using Helper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;

namespace SaveLoadMang
{
    public static class CsvRw
    {
        /// <summary>
        /// Öffnet einen Speicherdialog um übergebene Aufzeichnung abzuspeichern
        /// </summary>
        /// <param name="data">Aufzeichung zum speichern</param>
        /// <returns>Erfolg/Misserfolg</returns>
        public static bool SaveFile(RideDataList data)
        {
            try
            {
                SaveFileDialog saveDlg = new SaveFileDialog();
                saveDlg.Title = $"Speichern der Aufzeichnung vom {data.StartTime:dd.MM.yyyy} mit einer Samplerate von {data.SampleRate}";
                saveDlg.Filter = "CSV Files (*.csv)|*csv";
                saveDlg.AddExtension = true;
                if (saveDlg.ShowDialog() == true)
                {
                    string filename = saveDlg.FileName;
                    if (!filename.Contains(".csv"))
                    {
                        filename = filename + ".csv";
                    }

                    using (StreamWriter sw = new StreamWriter(filename))
                    {
                        List<string> decoded = ListToCsv(data);

                        sw.WriteLine(data.StartTime.ToString("dd.MM.yyyy") + ";" + data.SampleRate);

                        foreach (string item in decoded)
                        {
                            sw.WriteLine(item);
                        }
                    }
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                //MessageBox.Show("Fehler beim Speichern: " + exp.Message);
            }
            return false;
        }

        /// <summary>
        /// Öffnet einen Dialog um eine Datei auszuwählen und konvertiert den Inhalt über die CsvToList Methode.
        /// </summary>
        /// <returns>Konvertierte Daten</returns>
        public static RideDataList LoadFile()
        {
            try
            {
                OpenFileDialog openDlg = new OpenFileDialog();
                openDlg.Filter = "CSV Files (*.csv)|*csv";
                openDlg.Title = "Laden einer Aufzeichnung";

                if (openDlg.ShowDialog() == true)
                {
                    List<string> indata = new List<string>();

                    using (StreamReader sr = new StreamReader(openDlg.FileName))
                    {
                        while (!sr.EndOfStream)
                        {
                            indata.Add(sr.ReadLine());
                        }
                    }
                    return CsvToList(indata);
                }

                return null;
            }
            catch (Exception)
            {
                //MessageBox.Show("Fehler beim Lesen der Datei: " + exp.Message);
            }

            return null;
        }

        /// <summary>
        /// Konvertiert eine (aus CSV-Datei) gelesene List of string in eine Aufzeichnung
        /// </summary>
        /// <param name="encodeddata">Inhalt einer gelesenen csv-Datei</param>
        /// <returns>Daten als Aufzeichung</returns>
        private static RideDataList CsvToList(List<string> encodeddata)
        {
            var DecodedData = new RideDataList();
            int length = encodeddata.Count;
            for (int i = 0; i < length; i++)
            {
                string a = encodeddata[i];

                if (i != 0)
                {
                    DecodedData.Add(new RideDataSample(a));
                }
                else
                {
                    string[] tockens = a.Split(';');
                    DateTime time = Convert.ToDateTime(tockens[0]);
                    int sr = int.Parse(tockens[1]);
                    DecodedData.SampleRate = sr;
                    DecodedData.StartTime = time;
                }
            }
            return DecodedData;
        }

        /// <summary>
        /// serialisiert den Body der übergebenen Aufzeichnung
        /// </summary>
        /// <param name="decodeddata">Aufzeichnung zum serialisieren</param>
        /// <returns>Liste der serialisierten Aufzeichnungspunkten</returns>
        private static List<string> ListToCsv(RideDataList decodeddata)
        {
            var EncodedData = new List<string>();
            foreach (RideDataSample item in decodeddata)
            {
                EncodedData.Add(item.ToString());
            }
            return EncodedData;
        }
    }
}
